<p align="center">
    <a href="https://cqb325.gitee.io/cui-solid-doc">
        <img width="200" src="https://gitee.com/cqb325/cui-solid/raw/master/examples/assets/images/logo.svg">
    </a>
</p>

<h1>
CUI-PIN
    <h3>pin field based on solidjs</h3>
</h1>

[![CUI SolidJS](https://img.shields.io/npm/v/cui-pin.svg?style=flat-square)](https://www.npmjs.org/package/cui-pin)
[![NPM downloads](https://img.shields.io/npm/dm/cui-pin.svg?style=flat-square)](https://npmjs.org/package/cui-pin)
[![NPM downloads](https://img.shields.io/npm/dt/cui-pin.svg?style=flat-square)](https://npmjs.org/package/cui-pin)
![JS gzip size](https://img.badgesize.io/https:/unpkg.com/cui-pin/dist/cui-pin.min.esm.js?label=gzip%20size%3A%20JS&compression=gzip&style=flat-square)

## Install

    # npm
    npm install cui-pin
    # yarn
    yarn add cui-pin

## Demo

[Demo](https://codesandbox.io/p/sandbox/cui-pin-l38f5z "demo")

## Props
    
    // pin field length default:4
    length?: number
    // pin field type 'text'|'numeric'|'tel'|'password' default: 'numeric'
    type?: string
    // pin field inputMode default: 'tel'
    inputMode? "numeric"| "tel"| "text"| "none"| "url"| "email"| "decimal"| "search"
    // field value test regex default: /^[a-zA-Z0-9]+$/
    regexCriteria?: RegExp,
    // disabled pin default: false
    disabled?: boolean
    // pin complete callback
    onComplete?(pin: string, currentIndex: number): void