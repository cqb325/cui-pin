import { createSignal } from "solid-js";

export default function createModel(
    props: any,
    field: string,
    defaultValue: any
) {
    let value: Function;
    let setValue: Function;
    if (
        props[field] &&
        props[field].length === 2 &&
        typeof props[field][0] === "function"
    ) {
        value = props[field][0];
        setValue = props[field][1];
    } else {
        [value, setValue] = createSignal(props[field] || defaultValue);
    }
    return [value, setValue];
}
