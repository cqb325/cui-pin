export interface PinProps {
    length?: number;
    type?: string;
    inputMode?: "numeric" | "tel" | "text" | "none" | "url" | "email" | "decimal" | "search";
    onComplete?(pin: string, currentIndex: number): void;
    regexCriteria?: RegExp;
    disabled?: boolean;
    ref?: any;
}
export declare function Pin(props: PinProps): import("solid-js").JSX.Element;
