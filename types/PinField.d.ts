import { Signal } from "solid-js";
export interface PinFieldProps {
    type?: string;
    inputMode?: "numeric" | "tel" | "text" | "none" | "url" | "email" | "decimal" | "search";
    onBackspace?(index: number): void;
    index: number;
    currentIndex: number;
    onChange(v: string, index: number): void;
    onPaste?(value: string | undefined): void;
    value: Signal<string>;
    validate(value: string): string;
    disabled?: boolean;
}
export declare function PinField(props: PinFieldProps): import("solid-js").JSX.Element;
