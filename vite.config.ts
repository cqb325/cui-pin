import { defineConfig } from 'vite'
import solidPlugin from 'vite-plugin-solid'
import path from 'node:path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [solidPlugin()],
  server: {
    port: 3000,
  },
  build: {
    target: 'esnext',
    outDir: path.resolve(__dirname, './dist'),
    lib: {
      entry: path.resolve(__dirname, './src/index.tsx'),
      name: 'cui-pin'
    },
    rollupOptions: {
      context: 'globalThis',
      preserveEntrySignatures: 'strict',
      external: ['solid-js'],
      output: [
        {
          format: 'es',
          exports: 'named',
          sourcemap: false,
          entryFileNames: 'cui-pin.min.esm.js',
          chunkFileNames: '[name].js',
          assetFileNames: '[name].[ext]',
          namespaceToStringTag: true,
          inlineDynamicImports: false,
          manualChunks: undefined,
        }
      ]
    }
  },
  base: './'
});
