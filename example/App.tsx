import { createEffect, createSignal } from "solid-js";
import { Pin } from "../src";
import "./App.css";

function App() {
    let pin: any;
    const [dark, setDark] = createSignal(false);
    createEffect(() => {
        document.body.setAttribute('theme-mod', dark() ? 'dark' : 'light');
    })
    return (
        <div class="App">
            <input type="checkbox" checked={dark()} onClick={() => {
                setDark(!dark());
            }}/><label>{dark() ? 'LIGHT' : 'DARK'}</label>

            <div class="title">Numeric</div>
            <Pin ref={pin} onComplete={(pin: string, index: number) => {
                console.log(pin, index);
            }} />
            <button style={{'margin-top': '5px'}} onClick={() => {
                pin.clear();
            }}>CLEAR</button>
            <div class="title">Text</div>
            <Pin type='text' onComplete={(pin: string, index: number) => {
                console.log(pin, index);
            }} />
            <div class="title">Password</div>
            <Pin type='password' onComplete={(pin: string, index: number) => {
                console.log(pin, index);
            }} />
            <div class="title">Disabled</div>
            <Pin type='text' disabled/>

            <div class="title">RegexCriteria:/^[A-D]$/</div>
            <Pin type='text' regexCriteria={/^[A-D]$/}/>
        </div>
    );
}

export default App;
